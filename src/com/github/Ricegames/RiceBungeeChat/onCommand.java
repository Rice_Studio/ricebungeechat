package com.github.Ricegames.RiceBungeeChat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class onCommand extends Command {
	public onCommand(String args) {
		super(args);
	}

	public void execute(CommandSender sender, String[] args) {
		if ((!(sender instanceof ProxiedPlayer)) && (args.length == 1)) {
			if (args[0].equalsIgnoreCase("reload")) {
				sender.sendMessage(
						new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', "&c插件已重載!")).create());
				Main.m.loadConfig();
			}
			return;
		} else {
			if (!(sender instanceof ProxiedPlayer)) {
				sender.sendMessage(
						new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', "&c使用 rbc reload 重载插件"))
								.create());
			}
		}
	}
}
