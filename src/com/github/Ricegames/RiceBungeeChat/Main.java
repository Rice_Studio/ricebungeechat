package com.github.Ricegames.RiceBungeeChat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.io.ByteStreams;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.md_5.bungee.event.EventHandler;

public class Main extends Plugin implements Listener {
	public ConfigurationProvider cfg = ConfigurationProvider.getProvider(YamlConfiguration.class);
	public Configuration config;
	public static Main m;
	List<String> list = new ArrayList<String>();
	Map<ProxiedPlayer, Date> chat_time = new HashMap<ProxiedPlayer, Date>();
	Boolean enable_spam;

	public void onEnable() {
		m = this;
		loadConfig();
		getProxy().getPluginManager().registerListener(this, this);
		getProxy().getPluginManager().registerCommand(this, new onCommand("rbc"));
	}

	public void startThread() {
		Thread thread = new Thread() {
			public void run() {
				try {
					for (;;) {
						for (ProxiedPlayer p : Main.this.getProxy().getPlayers()) {
							if (Main.this.chat_time.containsKey(p)) {
								Main.this.chat_time.remove(p);
							}
						}
						Thread.sleep(30000L);
					}
				} catch (InterruptedException localInterruptedException) {
				}
			}
		};
		thread.start();
	}

	@EventHandler
	public void on(ChatEvent e) {
		if (!e.getMessage().startsWith("/")) {
			ProxiedPlayer p = (ProxiedPlayer) e.getSender();
			if (enable_spam && this.chat_time.containsKey(p) && getConfig().getInt("spam.sec") != 0) {
				long dif = getDateDifference((Date) this.chat_time.get(p));
				long sec =  getConfig().getInt("spam.sec") * 1000;
				if (dif > sec) {
					e.setMessage(detectionText(e.getMessage()));
					this.chat_time.put(p, getDate());
				} else {
					BigDecimal bd1 = new BigDecimal(sec - dif);
					BigDecimal bd2 = new BigDecimal(1000);
					String time = bd1.divide(bd2, 3, 4).toString();
					e.setCancelled(true);
					List<String> messages = getConfig().getStringList("messages");
					int sel = (int) (Math.random() * messages.size());
					String message = ((String) messages.get(sel)).replace("%time%", time);
					p.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', message)).create());
				}
			} else {
				e.setMessage(detectionText(e.getMessage()));
				this.chat_time.put(p, getDate());
			}
		}
	}

	@EventHandler
	public void on(PlayerDisconnectEvent e) {
		ProxiedPlayer p = e.getPlayer();
		if (this.chat_time.containsKey(p)) {
			this.chat_time.remove(p);
		}
	}

	public String detectionText(String Text) {
		for (String regexp : this.list) {
			Pattern r = Pattern.compile(regexp);
			Matcher m = r.matcher(Text);
			Text = m.replaceAll(getConfig().getString("replaceChar")).toString();
		}
		return Text;
	}

	public void loadConfig() {
		if (!getDataFolder().exists()) {
			getDataFolder().mkdir();
		}
		File configFile = new File(getDataFolder(), "config.yml");
		OutputStream os;
		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
				try {
					InputStream is = getResourceAsStream(configFile.getName());
					os = new FileOutputStream(configFile);
					ByteStreams.copy(is, os);
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					setConfig(this.cfg.load(configFile));
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.enable_spam = getConfig().getBoolean("spam.enable");
		this.list.clear();
		this.chat_time.clear();
		for (String regexp : getConfig().getStringList("regExpList")) {
			this.list.add(regexp);
		}
		for (String letter : getConfig().getStringList("fastWords")) {
			String[] args = letter.split("");
			if ((args.length != 0) && (args.length != 1)) {
				String regexp = "";
				int nowLetter = 0;
				while (args.length != nowLetter) {
					regexp = regexp + "(" + args[nowLetter] + "|" + args[nowLetter].toUpperCase() + ")";
					if (args.length != nowLetter + 1) {
						regexp = regexp + ".*";
					}
					nowLetter++;
				}
				this.list.add(regexp);
			}
		}
	}

	public Configuration getConfig() {
		return this.config;
	}

	public void setConfig(Configuration config) {
		this.config = config;
	}

	public static Date getDate() {
		return new Date();
	}

	public static long getDateDifference(Date PreviousDate) {
		return getDate().getTime() - PreviousDate.getTime();
	}
}